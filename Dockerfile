FROM python:3.10

# The enviroment variable ensures that the python output is set straight
# to the terminal with out buffering it first
ENV PYTHONUNBUFFERED 1

# create root directory for our project in the container
RUN mkdir /app

# Set the working directory to /jobs-website
WORKDIR /app

# copy requirements file
ADD ./requirements.txt /app/

# Install any needed packages specified in requirements.txt
RUN pip install -r requirements.txt

# Copy the app directory contents into the container at /app
ADD . /app/

RUN python manage.py migrate

CMD ["python", "manage.py", "runserver", "0.0.0.0:80"]

# keep container running
#CMD tail -f /dev/null